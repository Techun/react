import "./NavBar.css"

export default function NavBar() {
    return <ul className="navbar">
        <li className="navbar__element"><a href="#">Home</a></li>
        <li className="navbar__element"><a href="#">Menu</a></li>
        <li className="navbar__element"><a href="#">Company</a></li>
        <li className="navbar__element"><a href="#">Login</a></li>
    </ul>
}