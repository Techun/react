import "./BurgerBlcok.css"
import MainButton from "../MainButton/MainButton";

export default function BurgerBlock({src, name, price}) {
    return <div className="content-block">
        <img src={src} alt={name} className="content-block__image"/>
        <div className="content-block__item-wrapper">
            <div className="content-block__item-info">
                <span className="content-block__name">{name}</span>
                <span className="content-block__price">{price}</span>
            </div>
            <div className="content-block__item-description">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
            </div>
            <div className="content-block__item-cart">
                <div className="content-block__item-count">
                    1
                </div>
                <MainButton text="Add to cart"></MainButton>
            </div>
        </div>
    </div>
}