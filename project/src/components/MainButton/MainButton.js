import "./MainButton.css"

export default function ({notActive, text}) {
    const bgColor = notActive ? "transparent" : "rgba(53, 184, 190, 1)"
    const textColor = notActive ? "black" : "white"
    return <button className="main__button" style={{backgroundColor: bgColor, color: textColor}}>
        {text}
    </button>
}