import "./Header.css"
import logoImage from "../../assets/logo.svg"
import NavBar from "../NavBar/NavBar";
import Cart from "../Cart/Cart";

export default function Header() {
    return <header className="header">
        <img src={logoImage} alt="logo" className="logo"/>
        <div className="navbar-wrapper">
            <NavBar></NavBar>
            <Cart></Cart>
        </div>
    </header>
}
