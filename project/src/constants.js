import burgerDreams from "./assets/burger-dreams.png";
import burgerWaldo from "./assets/burger-waldo.png";
import burgerCali from "./assets/burger-cali.png";
import burgerBaconBuddy from "./assets/burger-bacon-buddy.png";
import burgerSpicy from "./assets/burger-spicy.png";
import burgerClassic from "./assets/burger-classic.png";

export const burgerData = [
    {src: burgerDreams, name: "Burger Dreams", price: "$ 9.20 USD"},
    {src: burgerWaldo, name: "Burger Waldo", price: "$ 10.00 USD"},
    {src: burgerCali, name: "Burger Cali", price: "$ 8.00 USD"},
    {src: burgerBaconBuddy, name: "Burger Bacon Buddy", price: "$ 9.99 USD"},
    {src: burgerSpicy, name: "Burger Spicy", price: "$ 9.20 USD"},
    {src: burgerClassic, name: "Burger Classic", price: "$ 8.00 USD"}
];

export const footerColumns = [
    ["COMPANY", "Home", "Order", "FAQ", "Contact"],
    ["TEMPLATE", "Style Guide", "Licence", "Webflow University"],
    ["FLOWBASE", "More Cloneables"]
]