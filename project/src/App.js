import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import Footer from "./components/Footer/Footer";
import {useEffect, useState} from "react";

export default function App() {
    return (
        <div className="App">
            <Header></Header>
            <Main></Main>
            <Footer></Footer>
        </div>
    );
}